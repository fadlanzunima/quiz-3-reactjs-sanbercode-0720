import React, {useEffect, useState} from "react";
import './MovieList.css'
import axios from "axios";

class ReadTitle extends React.Component {
    render() {
        return <td>{this.props.title}</td>
    }
}

class ReadYear extends React.Component {
    render() {
        return <td>{this.props.year}</td>
    }
}

class ReadDescription extends React.Component {
    render() {
        return <td>{this.props.description} </td>
    }
}

class ReadDuration extends React.Component {
    render() {
        return <td>{this.props.duration} menit</td>
    }
}

class ReadGenre extends React.Component {
    render() {
        return <td>{this.props.genre}</td>
    }
}

class ReadRating extends React.Component {
    render() {
        return <td>{this.props.rating}</td>
    }
}

function MovieList() {
    const [daftarMovie, setdaftarMovie] = useState(null)
    const [inputTitle, setInputTitle] = useState("")
    const [inputDescription, setInputDescription] = useState("")
    const [inputYear, setInputYear] = useState("")
    const [inputDuration, setInputDuration] = useState("")
    const [inputGenre, setInputGenre] = useState("")
    const [inputRating, setInputRating] = useState("")

    const [selectedId, setSelectedId] = useState(0)
    const [statusForm, setStatusForm] = useState("create")


    useEffect(() => {
        if (daftarMovie === null) {
            axios.get(`http://backendexample.sanbercloud.com/api/movies`)
                .then(response => {
                    setdaftarMovie(response.data.map(el => {
                        return {
                            id: el.id,
                            title: el.title,
                            description: el.description,
                            year: el.year,
                            duration: el.duration,
                            genre: el.genre,
                            rating: el.rating
                        }
                    }))
                })
        }
    }, [daftarMovie])

    const handleDelete = (event) => {
        let idMovie = parseInt(event.target.value)

        let newDaftarMovie = daftarMovie.filter(el => el.id !== idMovie)

        axios.delete(` http://backendexample.sanbercloud.com/api/movies/${idMovie}`)
            .then(res => {
                console.log(res)
            })

        setdaftarMovie([...newDaftarMovie])

    }

    const handleEdit = (event) => {
        let idMovie = parseInt(event.target.value)
        let newMovie = daftarMovie.find(x => x.id === idMovie)

        setInputTitle(newMovie.title)
        setInputDescription(newMovie.description)
        setInputYear(newMovie.year)
        setInputDuration(newMovie.duration)
        setInputGenre(newMovie.genre)
        setInputRating(newMovie.rating)

        setSelectedId(idMovie)
        setStatusForm("edit")
    }

    const handleChange = (event) => {
        const {name, value} = event.target

        switch (name) {
            case "inputTitle":
                setInputTitle(value);
                break;
            case "inputDescription":
                setInputDescription(value);
                break;
            case "inputYear":
                if (value.length <= 4) {
                    setInputYear(value);
                }
                break;
            case "inputDuration":
                setInputDuration(value);
                break;
            case "inputGenre":
                setInputGenre(value);
                break;
            case "inputRating":
                if (value.length <= 2) {
                    setInputRating(value);
                }
                break;
            default:
                break;
        }
    }

    const handleSubmit = (event) => {
        // menahan submit
        event.preventDefault()

        let title = inputTitle
        let description = inputDescription
        let year = inputYear
        let duration = inputDuration
        let genre = inputGenre
        let rating = inputRating

        if (title.replace(/\s/g, '') !== "" && description.toString().replace(/\s/g, '') !== "" && year !== "" && duration !== "" && genre !== "" && rating !== "") {
            if (statusForm === "create") {
                axios.post(`http://backendexample.sanbercloud.com/api/movies`, {
                    title,
                    description,
                    year,
                    duration,
                    genre,
                    rating
                })
                    .then(response => {
                        setdaftarMovie([...daftarMovie, {
                            title: title,
                            description: description,
                            year: year,
                            duration: duration,
                            genre: genre,
                            rating: rating
                        }])
                    })
            } else if (statusForm === "edit") {
                axios.put(` http://backendexample.sanbercloud.com/api/movies/${selectedId}`, {
                    title,
                    description,
                    year,
                    duration,
                    genre,
                    rating
                })
                    .then(res => {
                        let dataMovie = daftarMovie.find(el => el.id === selectedId)
                        dataMovie.title = title
                        dataMovie.description = description
                        dataMovie.year = year
                        dataMovie.duration = duration
                        dataMovie.genre = genre
                        dataMovie.rating = rating
                        setdaftarMovie([...daftarMovie])
                    })
            }

            setStatusForm("create")
            setSelectedId(0)
            setInputTitle("")
            setInputDescription("")
            setInputYear("")
            setInputDuration("")
            setInputGenre("")
            setInputRating("")
        }
    }

    return (
        <section>
            <h1>Table Film</h1>
            <table>
                <thead>
                <tr>
                    <th>No</th>
                    <th>Title</th>
                    <th>Deskripsi</th>
                    <th>Tahun</th>
                    <th>Deskripsi</th>
                    <th>Genre</th>
                    <th>Rating</th>
                    <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                {daftarMovie !== null && daftarMovie.map((daftar, index) => {
                    return (
                        <tr key={index}>
                            <td>{index + 1}</td>
                            <ReadTitle title={daftar.title}/>
                            <ReadDescription description={daftar.description}/>
                            <ReadYear year={daftar.year}/>
                            <ReadDuration duration={daftar.duration}/>
                            <ReadGenre genre={daftar.genre}/>
                            <ReadRating rating={daftar.rating}/>
                            <td>
                                <button onClick={handleEdit} value={daftar.id}>Edit</button>
                                &nbsp;
                                <button onClick={handleDelete} value={daftar.id}>Delete</button>
                            </td>
                        </tr>
                    )
                })}
                </tbody>
            </table>
            <form style={{marginTop: "1rem",background: "darkgrey", padding: "2rem"}} onSubmit={handleSubmit}>
                <h1>Form Daftar Film</h1>
                <div>
                    <label>Title :</label>
                    <input type={"text"} onChange={handleChange} name="inputTitle" value={inputTitle}/>
                </div>
                <div>
                    <label>Deskripsi :</label>
                    <textarea type={"text"} onChange={handleChange} name="inputDescription" value={inputDescription}></textarea>
                </div>
                <div>
                    <label>Tahun :</label>
                    <input type={"number"} onChange={handleChange} name="inputYear" value={inputYear}/>
                </div>
                <div>
                    <label>Durasi :</label>
                    <input type={"number"} onChange={handleChange} name="inputDuration" value={inputDuration}/>
                </div>
                <div>
                    <label>Genre :</label>
                    <input type={"text"} onChange={handleChange} name="inputGenre" value={inputGenre}/>
                </div>
                <div>
                    <label>Rating :</label>
                    <input type={"number"} min="1" max="10" onChange={handleChange} name="inputRating" value={inputRating}/>
                </div>
                <button type="submit" value="submit">Submit</button>
            </form>
        </section>
    )
}

export default MovieList