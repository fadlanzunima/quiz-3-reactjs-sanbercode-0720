import React, {useContext} from "react";
import {LoginContext, LoginProvider} from "./LoginContext";

function Login() {
    const [asLogin, setAsLogin] = useContext(LoginContext)

    const handleClick = (e) =>{
        e.preventDefault();
        setAsLogin("inline-block")
    }

    return(
        <LoginProvider>
        <section>
            <form>
                <h1>Login Form</h1>
                <div>
                    <label>Username</label>
                    <input type={"text"}/>
                </div>
                <div>
                    <label>Password</label>
                    <input type={"password"}/>
                </div>
                <button onClick={handleClick}>Submit</button>
                <br/>
                <span><b><i>Note : Klik tombol submit maka menu Movie List Editor akan muncul</i></b></span>
            </form>
        </section>
        </LoginProvider>
    )
}

export default Login