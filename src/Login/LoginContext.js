import React, { useState, createContext } from "react";

export const LoginContext = createContext();

export const LoginProvider = props => {
    const [asLogin, setAsLogin] = useState("none");

    return (
        <LoginContext.Provider value={[asLogin, setAsLogin]}>
            {props.children}
        </LoginContext.Provider>
    );

}