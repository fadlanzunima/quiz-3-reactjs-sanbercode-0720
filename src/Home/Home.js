import React, {useEffect, useState} from "react";
import './Home.css';
import axios from "axios"


class ReadTitle extends React.Component {
    render() {
        return <h3>{this.props.title}</h3>
    }
}

class ReadDescription extends React.Component {
    render() {
        return <li>Deskripsi: {this.props.description} </li>
    }
}

class ReadDuration extends React.Component {
    render() {
        return <li>Durasi: {this.props.duration}</li>
    }
}

class ReadGenre extends React.Component {
    render() {
        return <li>Genre: {this.props.genre}</li>
    }
}

class ReadRating extends React.Component {
    render() {
        return <li>Rating {this.props.rating}</li>
    }
}

function Home() {
    const [daftarMovie, setdaftarMovie] = useState(null)

    useEffect(() => {
        if (daftarMovie === null) {
            axios.get(`http://backendexample.sanbercloud.com/api/movies`)
                .then(response => {
                    setdaftarMovie(response.data.map(el => {
                        return {
                            id: el.id,
                            title: el.title,
                            description: el.description,
                            year: el.year,
                            duration: el.duration,
                            genre: el.genre,
                            rating: el.rating
                        }
                    }))
                })
        }
    }, [daftarMovie])

    return (
        <section>
            <h1>Daftar Film Terbaik</h1>
            <div>
                {daftarMovie !== null && daftarMovie.map((daftar, index) => {
                    return (
                        <ul key={index}>
                            <ReadTitle title={daftar.title}/>
                            <ReadRating rating={daftar.rating}/>
                            <ReadDuration duration={daftar.duration}/>
                            <ReadGenre genre={daftar.genre}/>
                            <ReadDescription description={daftar.description} />
                        </ul>
                    )
                })}
            </div>
        </section>
    )
}

export default Home