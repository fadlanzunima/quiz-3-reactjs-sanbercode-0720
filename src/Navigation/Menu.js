import logo from "../logo.png";
import {Link} from "react-router-dom";
import React, {useContext} from "react";

import './nav.css'
import {LoginContext} from "../Login/LoginContext";

function Menu(){
    const [asLogin] = useContext(LoginContext)

    return(
        <header>
            <img id="logo" src={logo} alt="logo"/>
            <nav>
                <ul>
                    <li>
                        <Link to="/home">Home</Link>
                    </li>
                    <li>
                        <Link to="/about">About</Link>
                    </li>
                    <li style={{display: asLogin === "none" ? "none" : "inline-block"}}>
                        <Link to="/movie_list">Movie List Editor</Link>
                    </li>
                    <li>
                        <Link to="/login">Login</Link>
                    </li>
                </ul>
            </nav>
        </header>
    )
}
export default Menu
