import React from "react";
import {Switch, Link, Route} from "react-router-dom";


import About from "../About/About";
import Home from "../Home/Home";
import MovieList from "../MovieList/MovieList";
import {LoginProvider} from "../Login/LoginContext";
import Login from "../Login/Login";
import Menu from "./Menu";

const Routes = () => {

    return (
        <>
            <LoginProvider>
            <Menu />
            <Switch>
                <Route path="/home">
                    <Home />
                </Route>
                <Route path="/about">
                    <About />
                </Route>
                <Route path="/movie_list">
                    <MovieList />
                </Route>
                <Route path="/login">
                    <Login/>
                </Route>
            </Switch>
            </LoginProvider>
        </>
    );
};

export default Routes;
