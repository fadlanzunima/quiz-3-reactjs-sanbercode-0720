import React from "react";
import './About.css'

function About() {
    return (
        <section>
            <div className="wrapper-about">
                <h1>Data Peserta Sanbercode Bootcamp Reactjs</h1>
                <ol>
                    <li>
                        <strong>Nama</strong> : Fadlan Zunima
                    </li>
                    <li>
                        <strong>Email</strong> : fadlanz36@gmail.com
                    </li>
                    <li>
                        <strong>Sistem Operasi yang digunakan</strong> : Windows 10
                    </li>
                    <li>
                        <strong>Akun Gitlab</strong> : fadlanzunima
                    </li>
                    <li>
                        <strong>Akun Telegram</strong> : @alanzunima
                    </li>
                </ol>
            </div>
        </section>
    )
}

export default About